<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentNamDan2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_nam_dan2s', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->string('class');
            $table->string('subclass');
            $table->boolean('is_exits');
            $table->integer('count_join');
            $table->integer('count_result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_nam_dan2s');
    }
}
