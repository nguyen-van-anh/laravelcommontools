<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class ResultsExport implements FromCollection
{
    protected $data = [];
    function __construct($data) {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->data);
    }
    /**
     * Returns headers for report
     * @return array
     */
    public function headings(): array {
        return [
            "STT",
            "Họ tên",
            "Tỉnh/ TP",
            "Quận/ huyện",
            "Trường",
            "Lớp",
            "Điểm",
            "Thời gian",
            "Học sinh"
        ];
    }

    public function map($data): array {
        return $data;
    }
}
