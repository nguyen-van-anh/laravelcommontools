<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class ArrayExport implements FromCollection
{
    protected $data = [];
    function __construct($data) {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->data);
    }


    public function map($data): array {
        return $data;
    }
}
