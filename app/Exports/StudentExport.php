<?php

namespace App\Exports;

use App\Models\StudentNamDan2;
use Maatwebsite\Excel\Concerns\FromCollection;

class StudentExport implements FromCollection
{
    protected $class = [];
    function __construct($class) {
        $this->class = $class;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StudentNamDan2::where('class', $this->class)->get();
    }
}
