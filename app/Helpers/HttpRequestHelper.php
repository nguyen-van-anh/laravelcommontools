<?php
namespace App\Helpers;

class HttpRequestHelper
{
    public static function getContentWithTag($tagname, $resource)
    {
        $tagNameOpen = "<{$tagname}";
        $tagNameClose = "</{$tagname}>";
        return "<{$tagname}" . substr($resource, strpos($resource, $tagNameOpen) + strlen($tagNameOpen), strpos($resource, $tagNameClose) - strpos($resource, $tagNameOpen) - strlen($tagNameOpen)) . "</{$tagname}>";
    }

    public static function getContentOuterWithTag($tagname, $resource)
    {
        $tagNameOpen = "<{$tagname}>";
        $tagNameClose = "</{$tagname}>";
        return substr($resource, strpos($resource, $tagNameOpen) + strlen($tagNameOpen), strpos($resource, $tagNameClose) - strpos($resource, $tagNameOpen) - strlen($tagNameOpen));
    }


    public static function removeTag($tagname, $resource)
    {
        $tagNameOpen = "<{$tagname}>";
        $tagNameClose = "</{$tagname}>";
        $result_resource = substr($resource, 0, strpos($resource, $tagNameOpen));
        $result_resource = $result_resource . substr($resource, (strpos($resource, $tagNameClose) + strlen($tagNameClose)));
        return $result_resource;
    }

    public static function htmlToArrayData($contents)
    {
        $DOM = new DOMDocument;
		$DOM->loadHTML($contents);
		$items = $DOM->getElementsByTagName('tr');
		$return = array();
		foreach ($items as $node) {
			$return[] = self::tdrows($node->childNodes);
		}
		return $return;
    }
    static function tdrows($elements)
	{
		$str = array();
		foreach ($elements as $element) {
			$str[] = $element->nodeValue;
		}

		return $str;
	}
}
