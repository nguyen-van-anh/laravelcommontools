<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\HttpRequestHelper;
use App\Exports\ResultsExport;
use App\Exports\StudentExport;
use App\Exports\ArrayExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\StudentNamDan2;
use Illuminate\Support\Str;

class GetDataHocVaLamTheoLoiBac extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'craw:hocvalamtheoloibac {class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Craw data with HOCVALAMTHEOLOIBAC';

    protected $domain = "https://hocvalamtheobac.vn/";

    protected $result = "ket-qua";

    protected $studentjoin = "danh-sach-thi-sinh";

    protected $paramsquery = "?page=1&target=group_a&province_id=2&district_id=34&school_id=30049&class_id=10";

    protected $total_row_with_page = 50;

    protected $class = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->class = $this->argument("class");
        // Lấy dữ liêu lớp trong database
        $data_student = StudentNamDan2::where('class', $this->class)->get();
        // Bước 1: Lấy toàn bộ dữ liệu
        $all_data_result = $this->getAllData();
        // Reset data student
        StudentNamDan2::reset($data_student);
        // map data với student
        for ($index=0; $index < count($data_student); $index++) {
            for ($index_online=0; $index_online < count($all_data_result); $index_online++) {
                if(! isset($all_data_result[$index_online][8])) {
                    if($this->isStudentThatName($all_data_result[$index_online], $data_student[$index])) {
                        $all_data_result[$index_online][8] = $data_student[$index]->id;
                        $all_data_result[$index_online][9] = $data_student[$index]->class . $data_student[$index]->subclass;
                        $data_student[$index]->is_exits = true;
                        $data_student[$index]->count_result ++;
                        $this->info($data_student[$index]->id . " __ : __ " . $data_student[$index]->name . " __ : __ " . $data_student[$index]->class . $data_student[$index]->subclass . " __ : __ " . $data_student[$index]->count_result);
                    }
                }
            }
        }
        // Check false
        $data_null = [];
        for ($index_online=0; $index_online < count($all_data_result); $index_online++) {
            if(! isset($all_data_result[$index_online][8])) {
                // Kiểm tra trong chuỗi có lớp không
                $status_check = false;
                for ($class_number=1; $class_number <= 10 ; $class_number++) {
                    $string_1 = $this->class . "c" . $class_number;
                    $string_2 = "c" . $class_number . "k" . (57 + ($this->class - 10));
                    $name = strtolower($all_data_result[$index_online][1]);
                    if(strpos($name, $string_1) !== false) $status_check = true;
                    if(strpos($name, $string_2) !== false) $status_check = true;
                    if($status_check) {
                        $all_data_result[$index_online][8] = null;
                        $all_data_result[$index_online][9] = $this->class . "C" . $class_number;
                        break;
                    }
                }
                if($status_check == false) {
                    $data_null[] = $all_data_result[$index_online];
                    unset($all_data_result[$index_online]);
                }
            }
        }
        // save data
        for ($index=0; $index < count($data_student); $index++) {
            $data_student[$index]->save();
        }
        // Bước 2: Tạo file excel và nhét dữ liệu vào
        array_unshift($all_data_result, [
            "STT",
            "Họ tên",
            "Tỉnh/ TP",
            "Quận/ huyện",
            "Trường",
            "Lớp",
            "Điểm",
            "Thời gian",
            "ID Student",
            "Lớp Nam Đàn 2"
        ]);
        array_unshift($data_null, [
            "STT",
            "Họ tên",
            "Tỉnh/ TP",
            "Quận/ huyện",
            "Trường",
            "Lớp",
            "Điểm",
            "Thời gian",
            "ID Student",
            "Lớp Nam Đàn 2"
        ]);
        Excel::store(new ResultsExport($all_data_result), "data_result_{$this->class}.xlsx");
        Excel::store(new StudentExport($this->class), "data_student_{$this->class}.xlsx");
        Excel::store(new ArrayExport($data_null), "data_empty_{$this->class}.xlsx");
        // Bước 3: Lưu file excel vào máy
        return 0;
    }

    public function getAllData()
    {
        // Lấy tổng số dữ liệu
        $total = $this->getTotalResult();
        // Lấy tổng số trang
        // $total_page = 1;
        $total_page = ceil($total / $this->total_row_with_page);
        $total_data = [];
        for ($page_number=1; $page_number <= $total_page; $page_number++) {
            $data = $this->getResultTableWithPage($page_number);
            $total_data = array_merge($total_data, $data);
        }
        return $total_data;
    }

    public function getResultTableWithPage($page)
    {
        $request_page = $this->domain . $this->result . $this->getParamWithPage($page);
        $request_html_page = file_get_contents($request_page);
        $request_table = HttpRequestHelper::getContentWithTag("table", $request_html_page);
        // Lấy dòng .... thông tin của bảng
        $request_table_head = HttpRequestHelper::getContentWithTag("thead", $request_table);
        $data_header = [];
        for ($i=0; $i < 8; $i++) {
            $data_header[$i] = HttpRequestHelper::getContentOuterWithTag("th", $request_table_head);
            $request_table_head = HttpRequestHelper::removeTag("th", $request_table_head);
        }
        // Lấy nội dung của bảng
        $request_table_body = HttpRequestHelper::getContentWithTag("tbody", $request_table);
        $data_content_page = [];
        while ($request_table_body != "<tbody> </tbody>") {
            $request_table_body_tr = HttpRequestHelper::getContentWithTag("tr", $request_table_body);
            $request_table_body = HttpRequestHelper::removeTag("tr", $request_table_body);
            $request_table_body = preg_replace('/\s+/', ' ', $request_table_body);
            $data_body = [];
            for ($i=0; $i < 8; $i++) {
                $data_body[$i] = HttpRequestHelper::getContentOuterWithTag("td", $request_table_body_tr);
                $request_table_body_tr = HttpRequestHelper::removeTag("td", $request_table_body_tr);
            }
            $data_content_page[] = $data_body;
        }
        $this->info("Đã lấy được " . count($data_content_page) . " dòng trong trang " . $page);
        return $data_content_page;
    }

    public function getTotalResult() {
        $request_page = $this->domain . $this->result . $this->getParamWithPage(1);
        $request_html_page = file_get_contents($request_page);
        $pos = strpos($request_html_page, "Tổng số:");
        $string_total_html = substr ($request_html_page, $pos, 500);
        $string_totla = HttpRequestHelper::getContentOuterWithTag("span", $string_total_html);
        return (int) $string_totla;
    }

    public function isStudentThatName($data, $student)
    {
        $name_one = $this->slugify($data[1]);
        $name_true = $this->slugify($student->name);

        $name_sw_1 = $this->slugify($student->name . $student->class . $student->subclass);
        $name_sw_2 = $this->slugify($student->name . $student->subclass);


        if($name_one == $name_true) {
            return true;
        }

        if($name_one == $name_sw_1) {
            return true;
        }

        if($name_one == $name_sw_2) {
            return true;
        }

        if(strpos($name_one, $name_sw_1) !== false) {
            return true;
        }

        if(strpos($name_one, $name_sw_2) !== false) {
            return true;
        }

        if(strpos($name_sw_1, $name_one) !== false) {
            return true;
        }

        if(strpos($name_sw_2, $name_one) !== false) {
            return true;
        }

        if(strpos($name_one, $name_true) !== false) {
            return true;
        }

        if(strpos($name_true, $name_one) !== false) {
            return true;
        }

        return false;
    }

    public function getParamWithPage($page)
    {
        return str_replace(["?page=1&", "&class_id=10"], ["?page={$page}&", "&class_id={$this->class}"], $this->paramsquery);
    }
    public static function slugify($text)
    {
        $string = Str::slug($text);
        return str_replace('-', '', $string);
    }
}
