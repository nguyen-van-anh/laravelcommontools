<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use Illuminate\Support\Facades\Storage;
use File;

class ConvertController extends Controller
{
    public function convert(Request $request)
    {
        $number = $request->has('number') ? $request->get('number') : 0;
        /** get convert action in id */
        $model_actions = Question::select('id')->where('answer_media', 'like', 'data:image/png;base64%')->limit(100)->orderBy('id', 'desc')->get();
        if($model_actions != null) {
            foreach ($model_actions as $id_select) {
                $model_action = Question::find($id_select->id);
                $base64 = str_replace("data:image/png;base64,", "", $model_action->answer_media);
                $bin = base64_decode($base64);
                $size = getImageSizeFromString($bin);
                $ext = substr($size['mime'], 6);
                $img_file = "filename_image_save.{$ext}";
                Storage::disk('local')->put($img_file, $bin);
                $cur_second = strtotime(date('Y-m-d H:i:s'));
                $file_extension = $ext;
                $file_name = $img_file;
                $file_size = Storage::size($img_file);
                $filesizeMB = round($file_size / (1024*1024), 2);
                $minetype = $size['mime'];

                $new_file_name = '';

                $destinationPath = '/storage_public/azota_exam';
                while(true){
                    $new_file_name = md5($file_name).'_'.uniqid().strtotime(date('Y-m-d H:i:s'));
                    break;
                }
                $path = Storage::disk('s3')->putFileAs($destinationPath, storage_path("app/" . $file_name), $new_file_name.'.'.$file_extension, 'public');

                $file_path = 'https://wewiin.nyc3.cdn.digitaloceanspaces.com/'.$path;
                $model_action->answer_media = $file_path;
                $model_action->save();
                $number ++;
                \Log::info("Đã xong: " . $model_action->id);

            }
            print_r($model_actions->toArray()[0]);
            $vars = array_keys(get_defined_vars());
            for ($i = 0; $i < sizeOf($vars); $i++) {
                \Log::info("Giải phóng: " . sizeOf($vars));
                unset($vars[$i]);
            }
            unset($vars,$i);
            return redirect('?number=' . $number);
        }

        return "Đã hoàn thành nhé.";
    }
}
