<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentNamDan2 extends Model
{
    use HasFactory;

    public static function reset(& $datas)
    {
        for ($i=0; $i < count($datas); $i++) {
            $datas[$i]->is_exits = false;
            $datas[$i]->count_join = 0;
            $datas[$i]->count_result = 0;
        }
    }
}
